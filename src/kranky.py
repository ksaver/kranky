#!/usr/bin/env python2.7
#
#  kranky.py
#  ~~~~~~~~~
#  Simple dictionary generator, for using in brute force attacks.
#  ksaver 2015

import argparse
import itertools
import random
import string
import sys

# Many routers uses uppercase hexadecimal digits for WPA keys
default_charset = string.digits + string.uppercase[:6]

# And 8 digit lenght
default_lenght = 8
default_number = 10

def argument_parser(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--charset',
                        dest="charset",
                        type=str,
                        default=default_charset,
                        help="Characters to use in generated passwords.")
    parser.add_argument('-l', '--lenght',
                        dest="lenght",
                        type=int,
                        default=default_lenght,
                        help="Lenght of the generated passwords.")
    parser.add_argument('-r', '--random',
                        action="store_true",
                        default=False,
                        help="Generate random strings instead of sequential ones.")
    parser.add_argument('-n', '--number-of-passwords',
                        dest="num_passwords",
                        type=int,
                        default=default_number,
                        help="Number of passwords to generate. Use with -r or --random.")
    parser.add_argument('--prefix',
                        dest="prefix",
                        type=str,
                        default='',
                        help="A string to use as the beginning of generated passwords.")
    parser.add_argument('--suffix',
                        dest="suffix",
                        type=str,
                        default='',
                        help="A string to use as the ending of generated passwords.")
    parser.add_argument('--passwords-from-file',
                        dest="password_file",
                        default=None,
                        type=argparse.FileType('r'),
                        help="File to load passwords from.")

    arguments = parser.parse_args()
    return arguments

def bruteforce(charset, lenght, prefix='', suffix=''):
    for i in itertools.product(charset, repeat=lenght):
        password = ''.join(i)
        print "%s%s%s" % (prefix, password, suffix)

def random_string(charset, lenght, prefix='', suffix=''):
    if len(charset) < lenght:
        while (len(charset) < lenght):
            charset += charset
        charset = charset[:lenght]
    rnd_samp = random.sample(charset, lenght)
    password = "".join(rnd_samp)
    return "%s%s%s" % (prefix, password, suffix)

def main(arguments):
    # load data from arguments:
    num_pwd = arguments.num_passwords
    prefix  = arguments.prefix
    suffix  = arguments.suffix
    charset = ''.join(set(arguments.charset))
    lenght  = arguments.lenght - len(prefix) - len(suffix)

    if arguments.password_file:
        passwords = arguments.password_file.read().split()
        for password in passwords:
            print "%s%s%s" % (prefix, password, suffix)
        sys.exit(0)

    elif arguments.random:
        for n in range(num_pwd):
            print random_string(charset, lenght, prefix, suffix)
            
    else:
        bruteforce(charset, lenght, prefix, suffix)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.argv.append('--help')

    arguments = argument_parser(sys.argv)
    main(arguments)
